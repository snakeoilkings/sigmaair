/**
 * 
 * LatComparator allows for sorting by a City's Latitude
 * 
 * Homework 7
 * CSE 214
 * Recitation #6
 * TA: Kevin Flanygolts
 * Grader: Zheyuan (Jeffrey) Gao
 * 
 * @author Kathryn Blecher
 * email: kathryn.blecher@gmail.com
 * Stony Brook ID: 108871623
 */
import java.util.Comparator;


public class LatComparator implements Comparator<City> {
	/**
	 * overrides compare to sort by lat
	 */
	@Override
	public int compare(City o1, City o2) {
		City c1 = (City) o1;
        City c2 = (City) o2;
        LatLng l1 = c1.getLocation();
        LatLng l2 = c2.getLocation();
        int cityLat1 = (int) (l1.getLat() * 100000);
        int cityLat2 = (int) (l2.getLat() * 100000);
        if (cityLat1 == cityLat2)
            return 0;
        else if (cityLat1 > cityLat2)
            return 1;
        else
            return -1;
    }
}
