/**
 * 
 * SigmaAir runs the main functions of the SigmaAir driver, it can load cities
 * and connectiosn from text files and find their latitude and longitude.
 * 
 * It can neatly print out information, edit (remove or add more cities and 
 * connections) and more.
 * 
 * Homework 7
 * CSE 214
 * Recitation #6
 * TA: Kevin Flanygolts
 * Grader: Zheyuan (Jeffrey) Gao
 * 
 * @author Kathryn Blecher
 * email: kathryn.blecher@gmail.com
 * Stony Brook ID: 108871623
 */

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

import com.google.code.geocoder.*;
import com.google.code.geocoder.model.*;

public class SigmaAir implements Serializable{
	
	/**
	 * 
	 */
	public static ArrayList<City> cities = new ArrayList<City>();
	public static final int MAX_CITIES=100;
	public static double[][] connections;  //adjacency matrix, i to i = 0, and 
										   //if no path = infinity
	/**
	 * Empty Constructor
	 */
	public SigmaAir() {
		connections = new double[MAX_CITIES][MAX_CITIES];
		for(int i=0; i < MAX_CITIES; i++) {
			for (int j=0; j < MAX_CITIES; j++) {
				if (i == j)
					connections[i][j] = 0;
				else
					connections[i][j] = Double.POSITIVE_INFINITY;
			}
		}
	}
	/**
	 * First loads up city info, then adds a city
	 * @param city (name of a city, info is found through geocoder)
	 */
	public void addCity(String city) {
		getCityData();
		String addr = null;
	    double lat = 0;
	    double lng = 0;
		try {
		    Geocoder geocoder = new Geocoder();
		    GeocoderRequest geocoderRequest;
		    GeocodeResponse geocodeResponse;
		    geocoderRequest = new GeocoderRequestBuilder().setAddress(city).
		      getGeocoderRequest();
		    geocodeResponse = geocoder.geocode(geocoderRequest);
		    addr = geocodeResponse.getResults().get(0).getFormattedAddress();
		    lat = geocodeResponse.getResults().get(0).getGeometry().
		      getLocation().getLat().doubleValue();
		    lng = geocodeResponse.getResults().get(0).getGeometry().
		      getLocation().getLng().doubleValue();
		} catch (Exception e) { System.out.print("City doesn't exist"); }
		LatLng latLngCity = new LatLng(lat, lng);
		City newCity = new City(city, latLngCity,0,0);
		//checks to see if matching name and matching location
		boolean exists = false;
		int index = 0;
		if (cities.isEmpty())
			exists = false;
		else {
			for (int i=0; i < cities.size(); i++) {
				if (newCity.getName().equals(cities.get(i).getName())) {
					LatLng l1 = newCity.getLocation();
					LatLng l2 = cities.get(i).getLocation();
					double dist = LatLng.calculateDistance(l1, l2);
					if (dist < 1)
						exists = true;
				}
				if (cities.get(i).getIndexPos() >= index)
					index = cities.get(i).getIndexPos()+1;
			}
		}
		if (exists == false) {
			newCity.setIndexPos(index);
			cities.add(newCity);
			for (int i=0; i < cities.size(); i++) {
				cities.get(i).setCityCount(index);
			serializeCity();
			}
			System.out.print("\n" +city+ " has been added: (" +lat+ ", " + lng
			  + ")");
		}
	}
	/**
	 * Adds a connection SigmaAir
	 * @param cityFrom start
	 * @param cityTo destination
	 */
	public void addConnection(String cityFrom, String cityTo) {
		getCityData();
		for (int i=1; i < cities.size(); i++)
			if (cities.get(0).getName().equals(cities.get(i).getName()))
				for (int j = cities.size()-1; j >= i; j--)
					cities.remove(j);
		boolean srcExists = false;
		boolean destExists = false;
		int srcIndex = 0;
		int destIndex = 0;
		LatLng srcCity = new LatLng();
		LatLng destCity = new LatLng();
		for(int i=0; i < cities.size(); i++) {
			if (cityFrom.equals(cities.get(i).getName())) {
				srcExists = true;
				srcIndex = cities.get(i).getIndexPos();
				srcCity = cities.get(i).getLocation();
			}
		}
		for(int i=0; i < cities.size(); i++) {
			if (cityTo.equals(cities.get(i).getName())) {
				destExists = true;
				destIndex = cities.get(i).getIndexPos();
				destCity = cities.get(i).getLocation();
			}
		}
		if ((srcExists && destExists) && (srcIndex != destIndex)) {
			double distance = LatLng.calculateDistance(srcCity, destCity);
			connections[srcIndex][destIndex] = distance;
			connections[destIndex][srcIndex] = distance;
			serializeConnection();
			System.out.print("\n" +cityFrom+ " ---> " +cityTo+ " added: " 
			  +distance);
		}
		else {
			System.out.println("\nConnection Addition Unsuccessful.");
		}
		return;
	}
	/**
	 * Removes a connection from SigmaAir
	 * @param cityFrom the start
	 * @param cityTo the destination
	 */
	public void removeConnection(String cityFrom, String cityTo) {
		getCityData();
		for (int i=1; i < cities.size(); i++)
			if (cities.get(0).getName().equals(cities.get(i).getName()))
				for (int j = cities.size()-1; j >= i; j--)
					cities.remove(j);
		getConnectionData();
		boolean srcExists = false;
		boolean destExists = false;
		int srcIndex = 0;
		int destIndex = 0;
		LatLng srcCity = new LatLng();
		LatLng destCity = new LatLng();
		for(int i=0; i < cities.size(); i++) {
			if (cityFrom.equals(cities.get(i).getName())) {
				srcExists = true;
				srcIndex = cities.get(i).getIndexPos();
				srcCity = cities.get(i).getLocation();
			}
		}
		for(int i=0; i < cities.size(); i++) {
			if (cityTo.equals(cities.get(i).getName())) {
				destExists = true;
				destIndex = cities.get(i).getIndexPos();
				destCity = cities.get(i).getLocation();
			}
		}
		if ((srcExists && destExists) && (srcIndex != destIndex) && 
		  (connections[srcIndex][destIndex] != Double.POSITIVE_INFINITY)) {
			connections[srcIndex][destIndex] = Double.POSITIVE_INFINITY;
			connections[destIndex][srcIndex] = Double.POSITIVE_INFINITY;
			serializeConnection();
			System.out.print("\nConnection between " +cityFrom+ " and " +cityTo
			  + " was successfully removed.\n");
		}
		else {
			System.out.println("\nConnection Removal Unsuccessful.");
		}
		return;
	}
	/**
	 * Finds the shortest path through Floyd Warshall Algorithm, the 
	 * reconstruction is done by jumping from each intermediate until the end
	 * @param cityFrom the start
	 * @param cityTo the destination
	 * @return the shortest path string
	 */
	public String shortestPath(String cityFrom, String cityTo) {
		getCityData();
		for (int i=1; i < cities.size(); i++)
			if (cities.get(0).getName().equals(cities.get(i).getName()))
				for (int j = cities.size()-1; j >= i; j--)
					cities.remove(j);
		getConnectionData();
		double[][] dist = new double[MAX_CITIES][MAX_CITIES];
		int[][] next = new int[MAX_CITIES][MAX_CITIES];
		String path = new String ("");
		for(int i=0; i < MAX_CITIES; i++) {
			for (int j=0; j < MAX_CITIES; j++) {
					dist[i][j] = connections[i][j];
					if ((i==j) ||(connections[i][j]==Double.POSITIVE_INFINITY))
						next[i][j] = -1;
					else
						next[i][j] = i;
			}
		}
		for (int k=0; k < MAX_CITIES; k++) {
			for (int i=0; i < MAX_CITIES; i++) {
				for (int j = 0; j < MAX_CITIES; j++) {
					if ((dist[i][k] + dist[k][j]) < dist[i][j]) {
						dist[i][j] = dist[i][k] + dist[k][j];
						next[i][j] =  next[k][j];
					}
				}
			}
		}
		int fromIndex = 0;
		int toIndex = 0;
		for(int i=0; i < cities.size(); i++) {
			if (cityFrom.equals(cities.get(i).getName())) 
				fromIndex = i;
			if (cityTo.equals(cities.get(i).getName())) 
				toIndex = i;
		}
		double distance = 0;
		path = cities.get(fromIndex).getName();
		while ((next[fromIndex][toIndex] != -1) && (next[fromIndex][toIndex]
		  != fromIndex)) {
			path = path + " ---> " + cities.get(next[fromIndex][toIndex]).
			  getName();
			distance += dist[fromIndex][toIndex];
			toIndex = next[fromIndex][toIndex];
		}
		distance += dist[fromIndex][toIndex];
		if (distance == Double.POSITIVE_INFINITY) {
			path = path + " to " +cityTo+ " is not possible.\n";
			return path;
		}
		path = path + " ---> " + cityTo + ": "+ distance + "\n";
		return path;
	}
	/**
	 * prints out a neat chart of all cities in specified sort, returns to an
	 * index sort after (not necessary, I just prefer it this way)
	 * @param sort the key to which sort we perform
	 */
	public void printAllCities(int sort) {
		getCityData();
		for (int i=1; i < cities.size(); i++)
			if (cities.get(0).getName().equals(cities.get(i).getName()))
				for (int j = cities.size()-1; j >= i; j--)
					cities.remove(j);
		if (sort == 1)
			Collections.sort(cities, new NameComparator());
		if (sort == 2)
			Collections.sort(cities, new LatComparator());
		if (sort == 3)
			Collections.sort(cities, new LngComparator());
		String output = String.format("%-36s%-15s%-12s","City Name",
				"Latitude", "Longitude");
		System.out.println(output);
		System.out.println("--------------------------------------------------"
		  + "-------------");
		for(int i=0; i < cities.size(); i++) {
			output = String.format("%-36s%-15s%-12s",cities.get(i).getName(),
			  cities.get(i).getLocation().getLat(), cities.get(i).getLocation()
			    .getLng());
			System.out.println(output);
		}
		Collections.sort(cities, new indexPosComparator());
	}
	/**
	 * Neatly prints out all connections and their distances
	 */
	public void printAllConnections() {
		getCityData();
		getConnectionData();
		for (int i=1; i < cities.size(); i++)
			if (cities.get(0).getName().equals(cities.get(i).getName()))
				for (int j = cities.size()-1; j >= i; j--)
					cities.remove(j);
		getConnectionData();
		String[] connect = new String[(cities.size()*cities.size())/2];
		double[] distance = new double[cities.size()*cities.size()/2];
		int count = 0;
		int preventDoubles=0;
		String output = String.format("%-50s%-15s","Route",
				"Distance");
		System.out.println(output);
		System.out.println("--------------------------------------------------"
		  + "-------------");
		for(int i=0; i < cities.size(); i++) {
			for(int j=0+preventDoubles; j < cities.size(); j++) {
				if (connections[i][j] != 0 && connections[i][j] != Double.
				      POSITIVE_INFINITY) {
					connect[count] = cities.get(i).getName() + " ---> " + 
				      cities.get(j).getName();
					distance[count] = LatLng.calculateDistance(cities.get(i).
					  getLocation(), cities.get(j).getLocation());
					count++;
				}
			}
			preventDoubles++;
		}
		int i = 0;
		while (connect[i] != null) {
			output = String.format("%-50s%-15s",connect[i], distance[i]);
			System.out.println(output);
			i++;
		}
	}
	/**
	 * loads out all the cities from a given text file from path or finds path
	 * @param fileName (name of file)
	 * @throws FileNotFoundException if file doesn't exist
	 */
	public void loadAllCities(String fileName) throws FileNotFoundException {
		String name = System.getProperty("java.class.path");
		String[] paths = name.split(";");
		name = paths[0] + "\\" +fileName;
		File file = new File(name);
		if (!(file.exists())) {
			file = new File(fileName);
			if (!(file.exists())) {
				throw new FileNotFoundException("Can't Find File. Try Again");
			}
		}
		Scanner input = new Scanner(file);
		String cityName = new String("");
		while(input.hasNext()) {
			cityName = input.nextLine();
			addCity(cityName);
		}
		for (int i=1; i < cities.size(); i++)
			if (cities.get(0).getName().equals(cities.get(i).getName()))
				for (int j = cities.size()-1; j >= i; j--)
					cities.remove(j);
	}
	/**
	 * loads all connections from a given text file, can find filepath or be
	 * given one
	 * @param fileName the name of file
	 * @throws FileNotFoundException if file doesn't exist
	 */
  public void loadAllConnections(String fileName) throws FileNotFoundException{
		getCityData();
		for (int i=1; i < cities.size(); i++)
			if (cities.get(0).getName().equals(cities.get(i).getName()))
				for (int j = cities.size()-1; j >= i; j--)
					cities.remove(j);
		getConnectionData();
		String name = System.getProperty("java.class.path");
		String[] paths = name.split(";");
		name = paths[0] + "\\" +fileName;
		File file = new File(name);
		if (!(file.exists())) {
			file = new File(fileName);
			if (!(file.exists())) {
				throw new FileNotFoundException("Can't Find File. Try Again");
			}
		}
		Scanner input = new Scanner(file);
		String cityNames = new String("");
		while(input.hasNext()) {
			cityNames = input.nextLine();
			String[] connectionNames = cityNames.split(",");
			addConnection(connectionNames[0], connectionNames[1]);
		}
		serializeConnection();
	}
	/**
	 * saves a city to sigma_air.obj file
	 */
	public static void serializeCity() {
		try {
			String name = System.getProperty("java.class.path");
			String[] paths = name.split(";");
			name = paths[0] + "\\" +"sigma_air.obj";
			File file = new File(name);
			Files.deleteIfExists(file.toPath());
	        FileOutputStream fileOut = new FileOutputStream(name, false);
	        ObjectOutputStream out = new ObjectOutputStream(fileOut);
	        out.writeObject(cities);
	        out.close();
	        fileOut.close(); 
	    } catch(IOException i) { i.printStackTrace(); }
	}
	/**
	 * saves the connection data in sigma_air2.obj
	 */
	public static void serializeConnection() {
		try {
			String name = System.getProperty("java.class.path");
			String[] paths = name.split(";");
			name = paths[0] + "\\" +"sigma_air2.obj";
	        FileOutputStream fileOut = new FileOutputStream(name, false);
	        ObjectOutputStream out = new ObjectOutputStream(fileOut);
	        out.writeObject(connections);
	        out.close();
	        fileOut.close(); 
	    } catch(IOException i) { i.printStackTrace(); }
	}
	/**
	 * collects the data from the obj file (sigma_air.obj)
	 */
	@SuppressWarnings("unchecked")
	private static void getCityData() {
		String name = System.getProperty("java.class.path");
		String[] paths = name.split(";");
		name = paths[0] + "\\" +"sigma_air.obj";
		File file = new File(name);
		if (file.exists()) {
			try {
				FileInputStream fileIn = new FileInputStream(name);
	    		ObjectInputStream in = new ObjectInputStream(fileIn);
	        	cities = (ArrayList<City>) in.readObject();
	        	in.close();
	        	fileIn.close(); } 
	   		catch(IOException i) {
	         	i.printStackTrace();
	         	return; }
	    	catch(ClassNotFoundException c) {
	    	 	System.out.println("sigma_air.obj not found");
	         	c.printStackTrace();
	         	return;
	    	}
		}
	}
	/**
	 * collects the connection data from the obj file (sigma_air2.obj)
	 */
	@SuppressWarnings("unchecked")
	private static void getConnectionData() {
		String name = System.getProperty("java.class.path");
		String[] paths = name.split(";");
		name = paths[0] + "\\" +"sigma_air2.obj";
		File file = new File(name);
		if (file.exists()) {
			try {
	    		FileInputStream fileIn = new FileInputStream(name);
	    		ObjectInputStream in = new ObjectInputStream(fileIn);
	        	connections = (double[][]) in.readObject();
	        	in.close();
	        	fileIn.close(); } 
	   		catch(IOException i) {
	         	i.printStackTrace();
	         	return; }
	    	catch(ClassNotFoundException c) {
	    	 	System.out.println("sigma_air2.obj not found");
	         	c.printStackTrace();
	         	return;
	    	}
		}
	}
}
