/**
 * 
 * Name comparator allows for sorting of the cities by name
 * 
 * Homework 7
 * CSE 214
 * Recitation #6
 * TA: Kevin Flanygolts
 * Grader: Zheyuan (Jeffrey) Gao
 * 
 * @author Kathryn Blecher
 * email: kathryn.blecher@gmail.com
 * Stony Brook ID: 108871623
 */
import java.util.Comparator;

public class NameComparator implements Comparator<City> {
	/**
	 * overrides compare to compare Strings
	 */
	@Override
	public int compare(City o1, City o2) {
		City c1 = (City) o1;
		City c2 = (City) o2;
		return (c1.getName().compareTo(c2.getName()));
	}
}
