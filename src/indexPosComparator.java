/**
 * 
 * indexPosComparator allows for sorting by index
 * 
 * Homework 7
 * CSE 214
 * Recitation #6
 * TA: Kevin Flanygolts
 * Grader: Zheyuan (Jeffrey) Gao
 * 
 * @author Kathryn Blecher
 * email: kathryn.blecher@gmail.com
 * Stony Brook ID: 108871623
 */
import java.util.Comparator;


public class indexPosComparator implements Comparator<City>{
	/**
	 * overrides compare to provide sorting by indexPos
	 */
	@Override
	public int compare(City o1, City o2) {
		City c1 = (City) o1;
        City c2 = (City) o2;
        int index1 = c1.getIndexPos();
        int index2 = c2.getIndexPos();
        if (index1 == index2)
            return 0;
        else if (index1 > index2)
            return 1;
        else
            return -1;
    }

}
