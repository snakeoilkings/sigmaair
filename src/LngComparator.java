/**
 * 
 * LngComparator allows for sorting by Longitude of city
 * 
 * Homework 7
 * CSE 214
 * Recitation #6
 * TA: Kevin Flanygolts
 * Grader: Zheyuan (Jeffrey) Gao
 * 
 * @author Kathryn Blecher
 * email: kathryn.blecher@gmail.com
 * Stony Brook ID: 108871623
 */
import java.util.Comparator;


public class LngComparator implements Comparator<City>{
	/**
	 * overrides the compare to sort by lng 
	 */
	@Override
	public int compare(City o1, City o2) {
		City c1 = (City) o1;
        City c2 = (City) o2;
        LatLng l1 = c1.getLocation();
        LatLng l2 = c2.getLocation();
        int cityLng1 = (int) (l1.getLng() * 100000);
        int cityLng2 = (int) (l2.getLng() * 100000);
        if (cityLng1 == cityLng2)
            return 0;
        else if (cityLng1 > cityLng2)
            return 1;
        else
            return -1;
    }

}
