/**
 * 
 * SigmaAirDriver runs the simulation
 * 
 * Homework 7
 * CSE 214
 * Recitation #6
 * TA: Kevin Flanygolts
 * Grader: Zheyuan (Jeffrey) Gao
 * 
 * @author Kathryn Blecher
 * email: kathryn.blecher@gmail.com
 * Stony Brook ID: 108871623
 */

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.util.Hashtable;
import java.util.Scanner;


public class SigmaAirDriver {

	public static void main(String[] args) throws IOException {
		int quit = 0;
		SigmaAir f = new SigmaAir();
		Scanner input = new Scanner(System.in);
		String in;
		while (quit != 1) {
			System.out.println("Menu:\n(A) Add City\n(B) Add Connection\n(C) Load "
		      + "all cities\n(D) Load all connections\n(E) Print all cities\n(F) "
		      + "Print all connections\n(G) Remove connection\n(H) Find shortest "
		      + "path\n(Q) Quit");
			System.out.print("Choose an option: ");
			in = input.next();
			char choice = in.charAt(0);
			switch(choice) {
		
				case 'A':	
				case 'a':	addCity(f);
							break;
				case 'B':	
				case 'b':	addConnection(f);
							break;
				case 'C':
				case 'c':	loadCities(f);
							break;
				case 'D':	
				case 'd':	loadConnections(f);
							break;
				case 'E':
				case 'e':	printCities(f);
							break;
				case 'F':
				case 'f':	f.printAllConnections();
							break;
				case 'G':
				case 'g':	removeConnections(f);
							break;
				case 'H':	
				case 'h':	findPath(f);
						    break;
		
				case 'Q':
				case 'q':	System.out.print("Program terminating...");
							quit = 1;
			}
		}
	}
	/**
	 * function call to print the cities
	 * @param f (SigmaAir)
	 */
	private static void printCities(SigmaAir f) {
		Scanner input = new Scanner(System.in);
		String in;
		System.out.print("\nA) Sort by name\nB) Sort by Latitude\nC) Sort by "
		  + "longitude\nQ) Return to main menu");
		System.out.print("\nChoose an option: ");
		in = input.next();
		char choice = in.charAt(0);
		switch(choice) {
			case 'A':	
			case 'a':	f.printAllCities(1);
						break;
			case 'B':	
			case 'b':	f.printAllCities(2);
						break;
			case 'C':
			case 'c':	f.printAllCities(3);
						break;
						
			case 'g':   f.printAllCities(4);
			break;
			case 'Q':
			case 'q':	return;
		}
	}
	/**
	 * function call to find the shortest path
	 * @param f (SigmaAir)
	 */
	private static void findPath(SigmaAir f) {
		Scanner input = new Scanner(System.in);
		System.out.print("\nName of first city: ");
		String city1 = input.nextLine();
		System.out.print("\nName of second city: ");
		String city2 = input.nextLine();
		System.out.print(f.shortestPath(city1, city2));	
		System.out.println();
	}
	/**
	 * function call to remove a connection 
	 * @param f (SigmaAir)
	 */
	private static void removeConnections(SigmaAir f) {
		Scanner input = new Scanner(System.in);
		System.out.print("\nName of first city: ");
		String city1 = input.nextLine();
		System.out.print("\nName of second city: ");
		String city2 = input.nextLine();
		f.removeConnection(city1, city2);
		System.out.println();
	}
	/**
	 * function call to load connections
	 * @param f (SigmaAir)
	 * @throws FileNotFoundException (if file doesn't exist)
	 */
	private static void loadConnections(SigmaAir f) throws 
	  FileNotFoundException {
		Scanner input = new Scanner(System.in);
		System.out.print("\nName of text file with connections: ");
		String name = input.nextLine();
		f.loadAllConnections(name);
		System.out.println();
	}
	/**
	 * function call to load cities
	 * @param f (SigmaAir)
	 * @throws FileNotFoundException if file does not exist
	 */
	private static void loadCities(SigmaAir f) throws FileNotFoundException {
		Scanner input = new Scanner(System.in);
		System.out.print("\nName of text file with cities: ");
		String name = input.nextLine();
		f.loadAllCities(name);
		System.out.println();
	}
	/**
	 * function call to add a connection 
	 * @param f (SigmaAir)
	 */
	private static void addConnection(SigmaAir f) {
		Scanner input = new Scanner(System.in);
		System.out.print("\nName of first city: ");
		String city1 = input.nextLine();
		System.out.print("\nName of second city: ");
		String city2 = input.nextLine();
		f.addConnection(city1, city2);
		System.out.println("\n");
	}
	/**
	 * function call to add a city
	 * @param f (SigmaAir)
	 */
	private static void addCity(SigmaAir f) {
		Scanner input = new Scanner(System.in);
		System.out.print("\nName of city: ");
		String name = input.nextLine();
		f.addCity(name);
		System.out.println();
	}
}