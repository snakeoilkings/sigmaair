/**
 * 
 * Creates the City object which stores the cities information
 * 
 * Homework 7
 * CSE 214
 * Recitation #6
 * TA: Kevin Flanygolts
 * Grader: Zheyuan (Jeffrey) Gao
 * 
 * @author Kathryn Blecher
 * email: kathryn.blecher@gmail.com
 * Stony Brook ID: 108871623
 */
import java.io.Serializable;

public class City implements Serializable{
	
	private String name;		  // city name
	private LatLng location;	  // Lat & Lng of city
	private int indexPos;		  // position in adj matrix
	private static int cityCount; // the number of cities in list
	/**
	 * Empty Constructor
	 */
	public City() {
		name = "";
		location = new LatLng(0,0);
		indexPos=0;
		cityCount = 0;		
	}
	/**
	 * Overloaded Constructor with all fields
	 * @param name
	 * @param location
	 * @param indexPos
	 * @param cityCount
	 */
	public City(String name, LatLng location, int indexPos, int cityCount) {
		this.name = name;
		this.location = location;
		this.indexPos = indexPos;
		this.cityCount = cityCount;
	}
	/**
	 * returns name of city
	 */
	public String getName() {
		return name;
	}
	/**
	 * Sets name of city
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * returns latitude and longitude of a city
	 */
	public LatLng getLocation() {
		return location;
	}
	/**
	 * Sets latitude and longitude of city
	 */
	public void setLocation(LatLng location) {
		this.location = location;
	}
	/**
	 * returns index position of a city
	 */
	public int getIndexPos() {
		return indexPos;
	}
	/**
	 * Sets index position of a city
	 */
	public void setIndexPos(int indexPos) {
		this.indexPos = indexPos;
	}
	/**
	 * returns cityCount of a city
	 */
	public int getCityCount() {
		return cityCount;
	}
	/**
	 * Sets cityCount of a city
	 */
	public void setCityCount(int cityCount) {
		this.cityCount = cityCount;
	}
}
